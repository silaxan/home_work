package programme.Bank;
public class BankAccount {
	public static final String bankName = "HNB";
	private String branch;
	private double accBalance = 0;
	private String accType;
	private String accNumber;
	private String accHolderName;
	
	public void OpenAccount(String branch, String accType, String accNumber, String accHolderName, int amount) {
		
		this.branch = branch;
		this.accType = accType;
		this.accNumber = accNumber;
		this.accHolderName = accHolderName;
		this.accBalance = amount;
		
	}
	
	public void withdrawal(double amount) {
		accBalance = accBalance - amount;
		
	}
	
	public void deposit(double amount) {
		accBalance = accBalance + amount;
		
	}
	
	public double getaccBalance() {
		return accBalance;
		
	}

	public String getaccHolderName() {
		return accHolderName;
	
	}
	
	public void getAccountDetails() {
		System.out.println("Bank Name :" + bankName + "-" + branch);
		System.out.println("Account Number :" + accNumber);
		System.out.println("Account type :" + accType);
		System.out.println("Account Holder Name :" + accHolderName);
		System.out.println("Account Balance :" + accBalance);
		System.out.println("----------------------------------");
		



		
	}
	
}
