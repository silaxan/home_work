package programme.Bank;
public class BankAccountDemo {
	public static void main(String args[]) {
		BankAccount acckonsikan = new BankAccount();
		BankAccount accmathi = new BankAccount();
		BankAccount accsilaxan = new BankAccount();
		
		acckonsikan.OpenAccount("point pedro", "Saving","897504983709", "J. konsikan", 20000);
		accmathi.OpenAccount("Jaffna","Saving","897504954349", "v.mathi", 30000);
		accsilaxan.OpenAccount("Nelliyadi","Saving","8975049845343", "S.silaxan", 25000);
		
		acckonsikan.getAccountDetails();
		accmathi.getAccountDetails();
		accsilaxan.getAccountDetails();
		
		acckonsikan.withdrawal(2000);
		accmathi.deposit(3000);
		accsilaxan.withdrawal(2000);
	}
}

/*
Bank Name :HNB-point pedro
Account Number :897504983709
Account type :Saving
Account Holder Name :J. konsikan
Account Balance :20000.0
----------------------------------
Bank Name :HNB-Jaffna
Account Number :897504954349
Account type :Saving
Account Holder Name :v.mathi
Account Balance :30000.0
----------------------------------
Bank Name :HNB-Nelliyadi
Account Number :8975049845343
Account type :Saving
Account Holder Name :S.silaxan
Account Balance :25000.0
----------------------------------

*/