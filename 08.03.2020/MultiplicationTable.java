public class MultiplicationTable {
    public static void main(String[] args) {
        int num = 10;
        System.out.print(" x ");
        for(int i = 1; i<=num;i++ ) {
            System.out.print("  "+i+" ");
        }
        System.out.println();
        System.out.println("-------------------------------------------");
         
        for(int i = 1 ;i<=num;i++) {
            System.out.print(" "+i+"|");
            for(int j=1;j<=num;j++) {
                System.out.print("  "+i*j+"|");
            }
            System.out.println();
        }
    }
     
}