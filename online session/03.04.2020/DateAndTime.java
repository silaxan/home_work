import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.ZoneId;


public class DateAndTime {
	public static void main(String[]args) {
		System.out.println("current Date and Time :");
		
		LocalDate date = LocalDate.now();
		System.out.println(date);
		
		LocalTime time = LocalTime.now();
		System.out.println(time);
		
		LocalDateTime datetime = LocalDateTime.now();
		System.out.println(datetime);
		
		ZoneId zoneIdNY = ZoneId.of("America/New_York");
		LocalTime NYtime = LocalTime.now(zoneIdNY);
		System.out.println("New_York :" + NYtime);
		
		ZoneId zoneIdSin = ZoneId.of("Singapore");
		LocalTime Sintime = LocalTime.now(zoneIdSin);
		System.out.println("Singapore :" + Sintime);
		
		
	}
}
