public class FromIndex {

   public static void main(String args[]) {
      String j = new String("Java is a Good Programming Language");
      System.out.println("Index value :" +  j.indexOf( 'a', 4 ));
   }
}
            //or

public class FromIndex {

   public static void main(String args[]) {
      String j = new String("Java is a Good Programming Language");
      System.out.print("Index :" );
      System.out.println(j.indexOf( 'a', 4 ));
   }
}

//output
//Index value :8