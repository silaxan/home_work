
public class ConcatMethod {
	   public static void main(String args[]) {
		  String java = "Java is a Good";
		  java = java.concat("Programming Language");
		System.out.println(" string Concat Method Value : " + java);

	   }

}

// Output
//string Concat Method Value : Java is a GoodProgramming Language
