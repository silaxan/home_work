
public class IndexReturnValueMethod {
	public static void main(String args[]) {
	      String java = "Java is a Good Programming Language";
	      int x = (java.indexOf('s'));
	      System.out.print(" string index Return Value :" +x);		
	}
}
				//or

public class IndexReturnValueMethod {
	public static void main(String args[]) {
	      String java = "Java is a Good Programming Language";
	      System.out.print(" string index Return Value :" );
	      System.out.println(java.indexOf('s'));

		
	}
}

//output
// string index Return Value :6