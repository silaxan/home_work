public class Index34 {

   public static void main(String args[]) {
      String s = new String("Java is a Good Programming Language");
      System.out.print("Value :" );
      System.out.println(s.startsWith("Java") );
      System.out.print("Value :" );
      System.out.println(s.startsWith("a") );
   }
}
//output
//Value :true
//Value :false
